<?php


namespace Ucc\Controllers;


use Ucc\Services\QuestionService;
use Ucc\Session;
use KHerGe\JSON\JSON;

class QuestionsController
{
    private QuestionService $questionService;
	private JSON $json;
	
    public function __construct(QuestionService $questionService)
    {
                    //parent::__construct();
        $this->questionService = $questionService;
    }

	 public function json($json_data)
    {
        $json = new JSON();
        $response = $json->encode($json_data);
		print_r($response);
        return  $response;
    }
	
	//Player's first Landing Page
    public function landingPage()
    {
		header("Location: /landing_page.php");
	}
	
    public function beginGame(): bool
    {	
	//Get player's name
		if ( isset( $_POST['submit'] ) ) 
		{
			$name= $_REQUEST['name'];
		}
        if (empty($name)) 
		{
			return $this->json(['response' => true, 'message' => 'You must first begin a game', 'status_code' => 400 ]);
        }
		
		//Store name and question count into session
        Session::set('name', $name);
        Session::set('questionCount', 1);
		
        //TODO Get first question for user
		$question = $this->questionService->getRandomQuestions();
		
		//return questions to player
        return    $this->json(['response' => true, 'question' => $question, 'status_code' => 201 ]);
    }

    public function answerQuestion(int $id): bool {
        if ( Session::get('name') === null ) {
            return $this->json(['response' => true, 'message' => 'You must first begin a game', 'status_code' => 400]);
        }

        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json(['response' => true, 'message' => 'You must provide an answer', 'status_code' => 400]);
        }

        //TODO: Check answer and increment user's points. Reply with a proper message
        $message = '';
        $question = null;

        return $this->json(['message' => $message, 'question' => $question]);
    }
}