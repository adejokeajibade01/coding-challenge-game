<?php


namespace Ucc\Controllers;


class Controller
{
    protected $requestBody;

    public function __construct()
    {
        
		$data = file_get_contents('php://input');
        $this->requestBody = json_decode($data);
		
		$newQuestions = file_get_contents('questions.json');
		$this->questions = json_decode($newQuestions);
    }
}